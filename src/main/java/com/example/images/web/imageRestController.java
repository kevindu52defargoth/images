package com.example.images.web;

import com.example.images.dao.ImageRepository;
import com.example.images.dao.KeyWordRepository;
import com.example.images.entities.Image;
import com.example.images.entities.KeyWord;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IM4JavaException;
import org.im4java.core.IMOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.io.FilenameUtils;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@RestController
@CrossOrigin("*")
public class imageRestController {
    @Autowired
    private ImageRepository imageRepository;
    @Autowired
    private KeyWordRepository keyWordRepository;

    private String repertoire = System.getProperty("user.home") + "/images/datas";

    @GetMapping("/search/{mot}")
    public List<Image> searchKeyWord(@PathVariable (name = "mot") String mot) throws Exception{
        //KeyWord keyWord = keyWordRepository.findById(Id).get();
        if (checkKeyWord(mot)) {
            List<KeyWord> keyWords = keyWordRepository.findByMot(mot);
            return (List<Image>) keyWords.get(0).getImages();
        } else {
            return Collections.emptyList();
        }
    }

    @GetMapping("/allImages")
    public List<Image> allImages() {
        return  imageRepository.findAll();
    }

    @GetMapping(path = "/preview/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] preview(@PathVariable (name="id") Long id) throws Exception{
        Image image = imageRepository.findById(id).get();
        String name = image.getFileName();
        File file = new File(repertoire + "/preview/" + name + ".jpg");
        Path path = Paths.get(file.toURI());
        return Files.readAllBytes(path);
    }

    @GetMapping(path = "/image/{name}", produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_GIF_VALUE, MediaType.IMAGE_PNG_VALUE})
    public byte[] afficherImage(@PathVariable (name="name") String name) throws Exception{
        File file = new File(repertoire + "/images/" + name);
        Path path = Paths.get(file.toURI());
        return Files.readAllBytes(path);
    }

    @GetMapping(path = "/checkKeyWord/{mot}")
    public boolean checkKeyWord(@PathVariable (name="mot") String mot) throws Exception{
        List<KeyWord> keyWords = keyWordRepository.findByMot(mot);
        return !keyWords.isEmpty();
    }

    @PostMapping(value = "/ajoutKeyword")
    public void ajoutKeyword(@RequestParam("idImage") long id, @RequestParam("motClef") String motClef) throws IOException, InterruptedException, IM4JavaException, NoSuchAlgorithmException {
        Image image = imageRepository.findById(id).get();
        Collection<KeyWord> l = image.getKeyWords();
        List<KeyWord> presents = keyWordRepository.findByMot(motClef);
        if (presents.isEmpty()){
            KeyWord keyWord = new KeyWord();
            keyWord.setLanguage("FR_fr");
            keyWord.setMot(motClef);
            keyWord.setCreationDate(new Date());
            keyWordRepository.save(keyWord);
            presents.add(keyWord);
        };

        KeyWord mot = presents.get(0);

        if (!l.contains(mot)){
            l.add(mot);
        }

        image.setKeyWords(l);

        imageRepository.save(image);
    }


    @PostMapping(value = "/uploadAPI")
    public void uploadIMage(@RequestParam("file[]") MultipartFile [] files, @RequestParam("motsClefs[]") String [] motsClefs ) throws IOException, InterruptedException, IM4JavaException, NoSuchAlgorithmException {

        // mots clefs

        List<KeyWord> keyWords = new ArrayList<>();

        for (String mot : motsClefs) {
            List<KeyWord> presents = keyWordRepository.findByMot(mot);

            if (presents.isEmpty()) {    // si le mot clef n'es pas déjà dans la base de nonnées, on l'y ajoute
                KeyWord keyWord = new KeyWord();
                keyWord.setLanguage("FR_fr");
                keyWord.setMot(mot);
                keyWord.setCreationDate(new Date());
                keyWordRepository.save(keyWord);
                keyWords.add(keyWord);
            } else {
                keyWords.add(presents.get(0));
            }
        }

        //images

        ConvertCmd cmd = new ConvertCmd();
        IMOperation op = new IMOperation();
        op.addImage();
        op.resize(1000,1000);
        op.addImage();

        MessageDigest md = MessageDigest.getInstance("MD5");

        for (MultipartFile file : files) {

            String filename = file.getOriginalFilename();
            String name = FilenameUtils.getBaseName(filename).substring(0, Math.min(29, FilenameUtils.getBaseName(filename).length()));
            String format = FilenameUtils.getExtension(filename);

            // calcul de la somme de controle à ajouter au nom du fichier
            String md5 = "azertyuiop";
            try {
                byte[] MD5digest = md.digest(file.getBytes());
                md5 = (new HexBinaryAdapter()).marshal(MD5digest).substring(0, 10);
            } catch (Exception e) {
                e.printStackTrace();
            }

            // création du fichier
            File transferFile = new File(repertoire + "/images/" + md5 + "/" + name + "." + format);
            transferFile.mkdirs();

            // on sauvegarde l'image
            try {
                file.transferTo(transferFile);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // ajout de l'image à la base de données
            Image image = new Image();
            image.setKeyWords(keyWords);
            image.setName(name);
            image.setFileName(md5 + "/" + name);
            image.setFormat(format);
            image.setCreationDate(new Date());
            image.setSize(file.getSize());

            imageRepository.save(image);

            // création de la preview
            new File(repertoire + "/preview/" + md5 ).mkdir();
            cmd.run(op, repertoire + "/images/" + md5 + "/" + name + "." + format, repertoire + "/preview/" + md5 + "/" + name + ".jpg");
        }
    }
}

