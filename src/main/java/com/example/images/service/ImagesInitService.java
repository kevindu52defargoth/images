package com.example.images.service;

public interface ImagesInitService {
    public void initFilms();
    public void initFirefox();
    public void initConfus();
}
