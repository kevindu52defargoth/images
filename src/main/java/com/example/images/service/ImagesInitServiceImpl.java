package com.example.images.service;

import com.example.images.dao.ImageRepository;
import com.example.images.dao.KeyWordRepository;
import com.example.images.entities.Image;
import com.example.images.entities.KeyWord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Array;
import java.util.*;
import java.util.stream.Stream;


@Service
@Transactional
public class ImagesInitServiceImpl implements ImagesInitService {

    @Autowired
    private ImageRepository imageRepository;
    @Autowired
    private KeyWordRepository keyWordRepository;

    @Override
    public void initFilms() {
        KeyWord keyWord = new KeyWord();
        keyWord.setLanguage("fr_FR");
        keyWord.setMot("film");
        keyWord.setCreationDate(new Date());
        keyWordRepository.save(keyWord);

        Stream.of("Game of throne", "12 hommes en colères", "le parrain", "forest Gump", "Hot fuzz", "Opération Lune").forEach( titre -> {
            String fileName = titre.replaceAll(" ", "_");
            Image image = new Image();
            image.setCreationDate(new Date());
            image.setName(titre);
            image.setFileName(fileName);
            image.setFormat("jpg");
            Collection<KeyWord> c = Collections.singleton(keyWord);
            image.setKeyWords(c);
            image.setPreview(true);
            imageRepository.save(image);
        });

        //List<Image> images = imageRepository.findAll();
        //keyWord.setImages(images);
        //keyWordRepository.save(keyWord);
    }

    public void initFirefox() {
        KeyWord keyWord = new KeyWord();
        keyWord.setLanguage("fr_FR");
        keyWord.setMot("firefox");
        keyWord.setCreationDate(new Date());
        keyWordRepository.save(keyWord);

        String[] fileNames = new String[] {"firefox0", "firefox1", "firefox2", "firefox3", "firefox4"};
        String[] types = new String[] {"svg", "png", "png", "png", "svg"};

        List<Image> images = new ArrayList<>();

        for(int i=0; i<fileNames.length; i++){
            Image image = new Image();
            image.setName(fileNames[i]);
            image.setFileName(fileNames[i]);
            image.setPreview(false);
            image.setFormat(types[i]);
            Collection<KeyWord> c = Collections.singleton(keyWord);
            image.setKeyWords(c);
            imageRepository.save(image);
            images.add(image);
        }

        //keyWord.setImages(images);
        //keyWordRepository.save(keyWord);
    }

    public void initConfus() {
        KeyWord keyWord = new KeyWord();
        keyWord.setLanguage("fr_FR");
        keyWord.setMot("confus");
        keyWord.setCreationDate(new Date());
        keyWordRepository.save(keyWord);

        Image image = new Image();
        image.setName("confus");
        image.setFileName("confus");
        image.setPreview(true);
        image.setFormat("webp");
        image.setCreationDate(new Date());
        image.setKeyWords(new ArrayList<>());
        imageRepository.save(image);

        image.getKeyWords().add(keyWord);

        KeyWord keyWord2 = new KeyWord();
        keyWord2.setLanguage("fr_FR");
        keyWord2.setMot("stickman");
        keyWord2.setCreationDate(new Date());
        keyWordRepository.save(keyWord2);

        image.getKeyWords().add(keyWord2);
        imageRepository.save(image);

    }
}
