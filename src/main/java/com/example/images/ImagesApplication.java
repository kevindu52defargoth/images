package com.example.images;

import com.example.images.service.ImagesInitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImagesApplication implements CommandLineRunner{

    @Autowired
    private ImagesInitService imagesInitService;

    public static void main(String[] args) {SpringApplication.run(ImagesApplication.class, args);}

    @Override
    public void run(String... args) throws Exception {
        imagesInitService.initFilms();
        imagesInitService.initFirefox();
        imagesInitService.initConfus();
    }

}
