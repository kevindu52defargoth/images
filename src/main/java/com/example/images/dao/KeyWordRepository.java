package com.example.images.dao;

import com.example.images.entities.KeyWord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@RepositoryRestResource
@CrossOrigin("*")
public interface KeyWordRepository extends JpaRepository<KeyWord, Long> {
    List<KeyWord> findByMot(String mot);
}
