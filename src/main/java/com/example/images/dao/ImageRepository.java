package com.example.images.dao;

import com.example.images.entities.Image;
import com.example.images.entities.KeyWord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.ArrayList;

@RepositoryRestResource
@CrossOrigin("*")
public interface ImageRepository extends JpaRepository<Image, Long> {
}
