package com.example.images.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Data @AllArgsConstructor @NoArgsConstructor
public class KeyWord {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;
    @Column(unique = true, length = 30)
    private String mot;
    @Column(length = 5)
    private String language;
    private Date creationDate;
    @ManyToMany(mappedBy = "keyWords")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Collection<Image> images;
}
