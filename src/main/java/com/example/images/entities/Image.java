package com.example.images.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Data @AllArgsConstructor @NoArgsConstructor
@Entity
public class Image implements Serializable {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(unique = true, length = 30)
    private String name;
    @Column(unique = true, length = 30)
    private String fileName;
    @Column(length = 4)
    private String format;
    private long size;
    private boolean preview;
    private Date creationDate;
    @ManyToMany()
    private Collection<KeyWord> keyWords;
}
